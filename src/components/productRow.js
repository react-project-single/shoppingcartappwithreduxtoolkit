import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  addToCart,
  decrementQuantity,
  incrementQuantity,
  removeItem,
} from '../redux/slices/counterSlices';

export default function ProductRow({product, cart}) {
  const dispatch = useDispatch();
  const counter = useSelector(state => state?.counter);
  // console.log(counter, 'counter');
  return (
    <View style={styles.card}>
      <View>
        {cart === true ? (
          <TouchableOpacity
            style={[
              styles.btn,
              {
                paddingHorizontal:5,
                zIndex: 1,
                height: 30,
                position:"absolute",
                alignSelf:"flex-end",
              },
            ]}
            onPress={() => dispatch(removeItem(product.id))}>
            <Text style={styles.titleBtn}>Remove</Text>
          </TouchableOpacity>
        ) : null}
        <Image source={{uri: product.image}} style={styles.image} />
      </View>
      <Text style={[styles.titleProduct]}>{product.title}</Text>
      <Text style={styles.price}>{product.price} $</Text>
      <>
        {cart !== true ? (
            <TouchableOpacity
              style={[
                styles.btn,
                {
                  height: 30,
                },
              ]}
              onPress={() => {
                dispatch(
                  addToCart({
                    id: product.id,
                    title: product.title,
                    quantity: product.quantity,
                    price: product.price,
                    image: product.image,
                  }),
                );
              }}>
              <Text style={styles.titleBtn}>Add To Cart</Text>
            </TouchableOpacity>
        ) : (
            <View style={[styles.spaceBetween,{height:30}]}>
              <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => {
                  dispatch(incrementQuantity(product.id));
                }}
                style={[styles.btn, {height: 25, width: 20}]}>
                <Text style={styles.pointer}>+</Text>
              </TouchableOpacity>
              <Text style={[styles.title, {paddingHorizontal: 20,color:"white"}]}>
                {product.quantity}
              </Text>
              <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => dispatch(decrementQuantity(product.id))}
                style={[styles.btn, {height: 25, width: 20}]}>
                <Text style={styles.pointer}>-</Text>
              </TouchableOpacity>
            </View>
        )}
      </>
    </View>
  );
}

const styles = StyleSheet.create({
  titleProduct: {
    color: '#18A558',
    fontWeight: '700',
    fontSize: 15,
    flex: 3,
  },
  price: {
    color: 'white',
    fontWeight: '700',
    fontSize: 15,
    marginVertical: 10,
  },
  spaceBetween: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  card: {
    margin: 15,
    borderWidth: 0.4,
    borderRadius: 15,
    borderColor: '#18A558',
    padding: 20,
    marginVertical: 20,
  },
  image: {height: 100,
     width: 100, 
    alignSelf: 'center',
     marginBottom: 20},
  // ==========
  title: {fontSize: 20, color: '#18A558'},
  titleBtn: {fontSize:15, color: 'white',fontWeight:"600"},
  btn: {
    backgroundColor: '#18A558',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pointer: {
    fontSize: 20,
    color:"white"
  },
});
