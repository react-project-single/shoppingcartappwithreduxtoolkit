import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {useSelector} from 'react-redux';
import ProductRow from '../components/productRow';

export default function home() {
  const counter = useSelector(state => state?.counter);
  const [productList, setProductList] = React.useState([]);
  React.useEffect(() => {
    fetch('https://fakestoreapi.com/products', {
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then(response => response.json())
      .then(data => {
        setProductList(data);
        // console.log(data);
      })
      .catch(err => {
        console.log(err.message);
      });
  }, []);
  return (
    <View style={{flex: 1, backgroundColor: 'black'}}>
      {/* <Text style={[styles.title, {marginVertical: 30}]}>home</Text> */}
      <View
        style={[
          styles.row,
          {alignSelf: 'center', alignItems: 'center', alignSelf: 'center'},
        ]}></View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        {productList ? (
          <FlatList
            style={{paddingBottom: 200}}
            data={productList}
            renderItem={({item: product}) => <ProductRow product={product} />}
            keyExtractor={product => product.id}
          />
        ) : (
          <ActivityIndicator size="small" color="#18A558" />
        )}
        <FlatList
          style={{paddingBottom: 200}}
          data={counter.cart}
          renderItem={({item: product}) => (
            <ProductRow product={product} cart={true} />
          )}
          keyExtractor={product => product.id}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {fontSize: 20, color: '#18A558'},
  btn: {
    height: 100,
    width: 100,
    backgroundColor: '#18A558',
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pointer: {
    fontSize: 30,
  },
});
