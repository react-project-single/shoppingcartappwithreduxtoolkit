import { configureStore } from "@reduxjs/toolkit";
import { cartReducer} from "../slices/counterSlices";

const store = configureStore({
    reducer:{
        counter:cartReducer
    }
})

export default store